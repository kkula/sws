<?php
//phpinfo();
//die;
if(!defined('_ENV_')) {
    if(getenv('APPLICATION_ENV')) {
        define('_ENV_',getenv('APPLICATION_ENV'));
    } else {
        define('_ENV_','students');
    }
}
$loader = require_once __DIR__ . '/../vendor/autoload.php';

// configuring autoloading modules.
$loader->add('Elections',__DIR__ . '/../project');

// new Silex Application
$app = new Silex\Application();

define('_ROOT',__DIR__ . '/..');

// configuring twig
$app->register(new Silex\Provider\TwigServiceProvider(),array(
    'twig.path' => __DIR__ . '/../templates'
));

// TODO: Twig filters.

// session provider
$app->register(new Silex\Provider\SessionServiceProvider());

// config provider
$app->register(new Igorw\Silex\ConfigServiceProvider(__DIR__ . '/../config/application.'._ENV_.'.yml'));

// project specific provider.
$app->register(new Elections\ProjectServiceProvider());


// default view data.
$app['view.name'] = '';
$app['view.vars'] = new stdClass();

$protected = function($req) use ($app) {
	// TODO: finish.
};

// middleware for session
$app->before(function() use ($app) {
	if($app['session']->has('user')) {
		$app['view.vars']->user = $app['session']->get('user');
	}
	if($app['debug']) {
		$app['view.vars']->debug['env'] = _ENV_;
		$app['view.vars']->debug['php'] = phpversion();
	}
});

// middleware for view variables from config file
$app->before(function() use ($app) {
    foreach($app['vvars'] as $key=>$val) {
        $app['view.vars']->{$key} = $val;
    }
});

// middleware for rendering
$app->after(function($req,$res) use ($app) {
	
	if(!$req->isXmlHttpRequest() && $app['view.name']){
            $res->setContent($app['twig']->render('views/'.$app['view.name'].'.twig', (array)$app['view.vars']));
        }
});

// controllers

$app->mount('',include '../controllers/about.php');
$app->mount('/elections',include '../controllers/elections.php');
$app->mount('/auth',include '../controllers/auth.php');
$app->mount('/admin',include '../controllers/admin.php');
$app->mount('',include '../controllers/extras.php');

$app->run();
