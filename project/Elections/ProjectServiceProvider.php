<?php

namespace Elections;

use Silex\Application;
use Silex\ServiceProviderInterface;

class ProjectServiceProvider implements ServiceProviderInterface
{   
   public function register(Application $app)
    {
        $config = $app['db'];
        $app['db.connection'] = Db\Base::connect($config);
        
        $app['db.users'] = $app->share(function() use ($app) {
            return new Db\Users();
        });
		
		$app['db.elections'] = $app->share(function() use ($app) {
            return new Db\Elections();
        });
    }
    
    public function boot(Application $app)
    {
        
    }
}