<?php

namespace Elections\Db;

class Elections extends Base
{
    public function getActiveElections($uid) {
		$stmt = $this->_db->prepare('SELECT *,has_voted(:usr,election_id) as has_v FROM active_elections');
		$stmt->execute(array('usr' => $uid));
		return $stmt->fetchAll();
	}
	
	public function getFutureElections() {
		return $this->_db->query('SELECT * FROM future_elections')->fetchAll();
	}
	
	public function getPastElections() {
		return $this->_db->query('SELECT *,is_public(election_id) as is_p FROM past_elections')->fetchAll();
	}
	
	public function getElections($id) {
		$sql = 'SELECT * FROM elections WHERE election_id = :id';
		$stmt = $this->_db->prepare($sql);
		$stmt->execute(array('id' => $id));
		return $stmt->fetch();
	}
	
	public function addElections($data) {
		$sql = 'INSERT INTO elections(name,description,start_date,end_date,candidates_deadline,places_limit,votes_limit)
			VALUES(:name,:description,:start_date,:end_date,:candidates_deadline,:places_limit,:votes_limit);';
		
		$stmt = $this->_db->prepare($sql);
		return $stmt->execute(array(
			'name' => $data['name'],
			'description' => $data['description'],
			'start_date' => $data['start_date'],
			'end_date' => $data['end_date'],
			'candidates_deadline' => $data['candidates_date'],
			'places_limit' => $data['places_limit'],
			'votes_limit' => $data['votes_limit']
		));
	}
	
	public function getAllElections() {
		return $this->_db->query('SELECT *,is_finished(election_id) AS is_f,is_public(election_id) as is_p FROM elections ORDER BY election_id;')->fetchAll();
	}
	
	public function addCandidate($elections, $candidate) {
		$sql = 'SELECT add_candidate(:index,:elections);';
		$stmt = $this->_db->prepare($sql);
		$stmt->execute(array(
			'elections' => $elections,
			'index' => $candidate
		));
		$ret = $stmt->fetch();
		return $ret[0];
	}
	
	public function getCandidates($id) {
		$sql = 'SELECT * FROM candidates_view WHERE election_id = :id';
		$stmt = $this->_db->prepare($sql);
		$stmt->execute(array(
			'id' => $id
		));
		return $stmt->fetchAll();
	}
	
	public function getAcceptedCandidates($id) {
		$sql = 'SELECT * FROM candidates_view WHERE election_id = :id AND is_accepted';
		$stmt = $this->_db->prepare($sql);
		$stmt->execute(array('id' => $id));
		return $stmt->fetchAll();
	}
	
	public function acceptCandidate($id) {
		$sql = 'SELECT accept_candidate(:id);';
		
		$stmt = $this->_db->prepare($sql);
		$stmt->execute(array('id' => $id));
		
		$ret = $stmt->fetch();
		
		return $ret[0];
	}
	
	public function hasVoted($user,$elections) {
		$sql = 'SELECT has_voted(:user,:el);';
		$stmt = $this->_db->prepare($sql);
		$stmt->execute(array(
			'user' => $user,
			'el' => $elections
		));
		
		$ret = $stmt->fetch();
		return $ret[0];
	}
	
	public function getCandidatesElection($candidate) {
		$sql = 'SELECT * FROM candidates WHERE candidate_id = :c';
		$stmt = $this->_db->prepare($sql);
		$stmt->execute(array('c' => $candidate));
		$row = $stmt->fetch();
		
		return $row['election_id'];
	}
	
	public function addVote($index,$candidate) {
		$sql = 'INSERT INTO votes(candidate_id,user_id) VALUES(:c,:v);';
		$stmt = $this->_db->prepare($sql);
		$stmt->execute(array(
			'c' => $candidate,
			'v' => $index
		));
	}
	
	public function getResults($id) {
		$sql = 'SELECT * FROM get_results(:id) ORDER BY rank;';
		
		$stmt = $this->_db->prepare($sql);
		$stmt->execute(array('id' => $id));
		return $stmt->fetchAll();
	}
	
	public function isFinished($eid) {
		$sql = 'SELECT is_finished(:id);';
		$stmt = $this->_db->prepare($sql);
		$stmt->execute(array('id' => $eid));
		$ret = $stmt->fetch();
		return $ret[0];
	}
	
	public function isPublished($eid) {
		$sql = 'SELECT is_public(:id);';
		$stmt = $this->_db->prepare($sql);
		$stmt->execute(array('id' => $eid));
		$ret = $stmt->fetch();
		return $ret[0];
	}
	
	public function publishElection($id) {
		$sql = 'UPDATE elections SET is_published = TRUE WHERE election_id = :id';
		$this->_db->prepare($sql)->execute(array('id' => $id));
	}
	
	public function unpublishElection($id) {
		$sql = 'UPDATE elections SET is_published = FALSE WHERE election_id = :id';
		$this->_db->prepare($sql)->execute(array('id' => $id));
	}
}