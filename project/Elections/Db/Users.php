<?php

namespace Elections\Db;

class Users extends Base
{
    public function auth($index_number,$password) {
		$password = hash('sha256', self::$_salt . $password);
		$query = 'SELECT authorize(:index,:pwd);';
		$stmt = $this->_db->prepare($query);
		$stmt->execute(array(
			'index' => $index_number,
			'pwd' => $password
		));
		$ret = $stmt->fetch();
		return $ret[0];
	}
	
	public function getUser($id) {
		$q = 'SELECT * FROM users WHERE index_number = :in';
		$stmt = $this->_db->prepare($q);
		$stmt->execute(array(
			'in' => $id
		));
		return $stmt->fetch();
	}
	
	public function getUsersPage($page) {
		$sql = 'SELECT * FROM users ORDER BY index_number LIMIT 50 OFFSET ' . (($page-1)*50);
		return $this->_db->query($sql)->fetchAll();
	}
	
	public function addUser($data) {
		$sql = 'INSERT INTO users(index_number,pesel,name,surname,password)
			VALUES(:index,:pesel,:name,:surname,:password);';
		
		$stmt = $this->_db->prepare($sql);
		return $stmt->execute(array(
			'index' => $data['index'],
			'pesel' => $data['pesel'],
			'name'  => $data['name'],
			'password' => hash('sha256',self::$_salt.$data['password']),
			'surname' => $data['surname']
		));
	}
	
	public function getLog() {
		$sql = 'SELECT * FROM log_view ORDER BY login_time DESC';
		return $this->_db->query($sql)->fetchAll();
	}
}