<?php

namespace Elections\Db;

/**
 * Base class for database connections.
 */
class Base {
    
    static protected $_pdo;
	static protected $_salt;
    /**
     * Pdo.
     * @var \PDO
     */
    protected $_db;
    
    public function __construct() {
        $this->_db = self::$_pdo;
    }
    
    static public function connect($config) {
        $dns = 'pgsql:dbname='.$config['database'] . ';host=' . $config['host'];
		self::$_salt = $config['salt'];

        try {
            return self::$_pdo = new \PDO($dns,$config['user'],$config['password']);
        } catch(Exception $e) {
            die('Cannot connect with database');
        }
    }
}