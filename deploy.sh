#!/bin/bash
rsync -avz \
 /var/www/wybory/ \
 -e ssh kk320786@students.mimuw.edu.pl:~/public_html/wybory/ \
 --exclude 'vendor' \
 --exclude 'web/.htaccess'
