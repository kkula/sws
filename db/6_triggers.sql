-- Logs all logins in 'log' table
CREATE OR REPLACE FUNCTION auth_login() RETURNS trigger AS $$
BEGIN

IF NEW.last_login = OLD.last_login THEN RETURN NEW;
ELSE
INSERT INTO log(index_number,login_time) VALUES(NEW.index_number,NEW.last_login);
RETURN NEW;
END IF;

END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER auth_log AFTER UPDATE ON users
FOR EACH ROW EXECUTE PROCEDURE auth_login();



-- Prevent adding new candidate if it's after candidates deadline.
CREATE OR REPLACE FUNCTION candidates_ddline() RETURNS trigger AS $$
BEGIN

IF (SELECT candidates_deadline FROM elections WHERE election_id = NEW.election_id) < NOW() THEN
	RAISE EXCEPTION 'Too late to register new candidate';
ELSE
	RETURN NEW;
END IF;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER cnddts_ddln BEFORE INSERT ON candidates
FOR EACH ROW EXECUTE PROCEDURE candidates_ddline();


-- prevent voting for candidate that isn't accepted
CREATE OR REPLACE FUNCTION prevent_vote_unacepted() RETURNS TRIGGER AS $$
BEGIN

IF EXISTS(SELECT * FROM candidates WHERE NOT is_accepted AND candidate_id = NEW.candidate_id) THEN
	RAISE EXCEPTION 'Cannot vote for unaccepted candidate.';
END IF;
RETURN NEW;

END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER prev_v_unac BEFORE INSERT OR UPDATE ON votes
FOR EACH ROW EXECUTE PROCEDURE prevent_vote_unacepted();




-- Prevent to vote if voting hasn't started or has finished.
CREATE OR REPLACE FUNCTION vote_on_elections_time() RETURNS TRIGGER AS $$
DECLARE
start_time TIMESTAMP;
end_time TIMESTAMP;

BEGIN
	start_time = (SELECT start_date FROM elections WHERE election_id = (SELECT election_id FROM candidates WHERE candidate_id = NEW.candidate_id));
	end_time = (SELECT end_date FROM elections WHERE election_id = (SELECT election_id FROM candidates WHERE candidate_id = NEW.candidate_id));
	IF(start_time < NOW() AND end_time > NOW()) THEN
		RETURN NEW;
	END IF;
	RAISE EXCEPTION 'Cannot vote right now';

END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER vote_on_e_time BEFORE INSERT OR UPDATE ON votes
FOR EACH ROW EXECUTE PROCEDURE vote_on_elections_time();


-- Prevent to add to many votes.
CREATE OR REPLACE FUNCTION votes_limit_check() RETURNS TRIGGER AS $$
DECLARE
users_votes INT;
votes_limit INT;
e_id INT;
BEGIN
	e_id = (SELECT election_id FROM candidates WHERE candidate_id = NEW.candidate_id);
	users_votes = (SELECT COUNT(*) FROM votes WHERE user_id = NEW.user_id AND candidate_id IN (SELECT candidate_id FROM candidates WHERE election_id = e_id));
	votes_limit = (SELECT elections.votes_limit FROM elections WHERE election_id = (SELECT election_id FROM candidates WHERE candidate_id = NEW.candidate_id));
	IF users_votes +1 <= votes_limit THEN
		RETURN NEW;
	END IF;
	RAISE EXCEPTION 'Za dużo głosów.';

END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER votes_lim_check BEFORE INSERT OR UPDATE ON votes
FOR EACH ROW EXECUTE PROCEDURE votes_limit_check();