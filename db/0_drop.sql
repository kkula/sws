DROP VIEW IF EXISTS elections_v,active_elections,past_elections,future_elections,users_v,log_view,candidates_view;
DROP TABLE IF EXISTS users, elections, candidates, votes, log CASCADE;
DROP SEQUENCE IF EXISTS elections_seq, votes_seq, candidates_seq;


--drop schema public cascade;
--CREATE SCHEMA public;