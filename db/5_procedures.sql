-- Authorize user. Returns True if creditalments are corrent, false otherwise
CREATE OR REPLACE FUNCTION authorize(index_nr varchar(64), passwd varchar(128) ) RETURNS boolean AS
$$

BEGIN

IF EXISTS (SELECT index_number FROM users WHERE users.index_number = index_nr AND users.password = passwd) THEN
	UPDATE users SET last_login = NOW() WHERE users.index_number = index_nr;
	RETURN TRUE;
ELSE
	RETURN FALSE;
END IF;
END;

$$ LANGUAGE 'plpgsql';


-- Returns ranking for elections.
CREATE OR REPLACE FUNCTION get_results(elec_id INT)
RETURNS TABLE(rank BIGINT,candidate_id INT,votes BIGINT,from_top_perc numeric,per numeric,name VARCHAR(64),surname VARCHAR(64),is_winner BOOLEAN) AS $$
BEGIN
RETURN QUERY
	WITH cte1 AS (
	SELECT *
		FROM votes
		WHERE votes.candidate_id IN
				(SELECT candidates.candidate_id
					FROM candidates
					WHERE election_id = elec_id)
	),
	CTE2 AS (
	SELECT cte1.candidate_id,
		COUNT(*) as votes,
		RANK() OVER(ORDER BY COUNT(*) DESC) as rank
	FROM cte1
	GROUP BY cte1.candidate_id
	ORDER BY votes DESC
	),
	CTE3 AS (
	SELECT cte2.rank,
		cte2.candidate_id,
		cte2.votes,
		SUM(cte2.votes) OVER(ORDER BY cte2.rank) as v_sum
	FROM cte2
),
CTE4 AS (
	SELECT cte3.rank,
		cte3.candidate_id,
		cte3.votes,
		100*v_sum/(SELECT COUNT(*) FROM cte1) as from_top_perc,
		100.0*cte3.votes/(SELECT COUNT(*) FROM cte1) as perc FROM CTE3
	)
SELECT cte4.*,
	users.name,
	users.surname,
	(SELECT places_limit FROM elections WHERE election_id = elec_id) >=cte4.rank
FROM cte4
	JOIN candidates
		ON cte4.candidate_id = candidates.candidate_id
	JOIN users
		ON users.index_number=candidates.user_id;
END;
$$ LANGUAGE plpgsql;




-- Adds new candidate to an elections.
CREATE OR REPLACE FUNCTION add_candidate(index_number VARCHAR(32),elections_id INTEGER) RETURNS BOOLEAN AS
$$
BEGIN
INSERT INTO candidates(election_id,user_id) VALUES(elections_id,index_number);
RETURN TRUE;

EXCEPTION
WHEN OTHERS THEN RETURN FALSE; 

END;
$$ LANGUAGE plpgsql;


-- Accepts candidate. Returns id elections id that candidate belongs to.
CREATE FUNCTION accept_candidate(id INTEGER) RETURNS INTEGER AS
$$
BEGIN

UPDATE candidates SET is_accepted = TRUE WHERE candidate_id = id;

RETURN (SELECT election_id FROM candidates WHERE candidate_id = id);

END;
$$ LANGUAGE plpgsql;


-- Checks if user voted in specified elections. Returns bool.
CREATE OR REPLACE FUNCTION has_voted(usr VARCHAR(32),elections INT) RETURNS BOOLEAN
AS $$
BEGIN
IF EXISTS (SELECT * FROM votes WHERE user_id = usr AND candidate_id IN (SELECT candidate_id FROM candidates WHERE election_id = elections)) THEN
	RETURN TRUE;
ELSE
	RETURN FALSE;
END IF;
END;
$$ LANGUAGE plpgsql;

-- If election is public
CREATE OR REPLACE FUNCTION is_public(el_id INTEGER) RETURNS BOOLEAN AS $$
BEGIN
	IF (SELECT end_date FROM elections WHERE election_id = el_id) < NOW() AND (SELECT is_published FROM elections WHERE election_id = el_id) THEN
		RETURN TRUE;
	ELSE
		RETURN FALSE;
	END IF;
END;

$$ LANGUAGE plpgsql;


-- If election is finished.
CREATE OR REPLACE FUNCTION is_finished(el_id INTEGER) RETURNS BOOLEAN AS $$
BEGIN
	IF (SELECT end_date FROM elections WHERE election_id = el_id) < NOW() THEN
		RETURN TRUE;
	ELSE
		RETURN FALSE;
	END IF;
END;

$$ LANGUAGE plpgsql;