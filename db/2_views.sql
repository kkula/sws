CREATE view elections_v AS (
SELECT *,
	(SELECT COUNT(*)
	FROM candidates AS c
	WHERE e.election_id = c.election_id
	AND c.is_accepted = true) as accepted_candidates,
	(SELECT COUNT(*)
	FROM candidates AS c
	WHERE e.election_id = c.election_id
	AND c.is_accepted = false) as rejected_candidates,
	(SELECT COUNT(*)
	FROM candidates AS c
		JOIN votes as v
		ON v.candidate_id=c.candidate_id
	WHERE e.election_id = c.election_id) as votes_count
FROM elections as e
);

CREATE VIEW active_elections AS (
	SELECT * FROM elections_v WHERE start_date < NOW() AND end_date > NOW()
);

CREATE VIEW past_elections AS (
	SELECT * FROM elections_v WHERE end_date < NOW()
);

CREATE VIEW future_elections AS (
	SELECT * FROM elections_v WHERE start_date > NOW()
);

CREATE VIEW users_v AS (
	SELECT * FROM users
);

CREATE VIEW log_view AS
(SELECT log.*,users.name,users.surname FROM log LEFT JOIN users ON log.index_number=users.index_number);


CREATE VIEW candidates_view AS (
SELECT candidates.*,users.name,users.surname FROM candidates LEFT JOIN users ON candidates.user_id=users.index_number
);