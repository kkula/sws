-- Sekwencje
CREATE SEQUENCE elections_seq START 50000;
CREATE SEQUENCE votes_seq START 50000;
CREATE SEQUENCE candidates_seq START 50000;



-- użytkownicy
CREATE TABLE users (
	index_number VARCHAR(32) PRIMARY KEY NOT NULL,
	pesel CHAR(11), -- może być null, np. dla obcokrajowców
	name VARCHAR(64) NOT NULL,
	surname VARCHAR(128) NOT NULL,
	password VARCHAR(256) NOT NULL, -- hasło hashowane, z solą.
	is_admin boolean NOT NULL DEFAULT false, -- czy użytkownik jest administratorem systemu
	last_login timestamp DEFAULT NULL,
	CHECK(
		pesel IS NULL OR
		(10 -
		((cast(substring(pesel from 1 for 1) as integer) +
		3*cast(substring(pesel from 2 for 1) as integer) +
		7*cast(substring(pesel from 3 for 1) as integer) +
		9*cast(substring(pesel from 4 for 1) as integer) +
		1*cast(substring(pesel from 5 for 1) as integer) +
		3*cast(substring(pesel from 6 for 1) as integer) +
		7*cast(substring(pesel from 7 for 1) as integer) +
		9*cast(substring(pesel from 8 for 1) as integer) +
		1*cast(substring(pesel from 9 for 1) as integer) +
		3*cast(substring(pesel from 10 for 1) as integer)) % 10)) % 10
		= cast(substring(pesel from 11 for 1) as integer)
	) -- sprawdzanie poprawności peselu
);

-- tworzymy częściowy, unikalny indeks dla peselu
CREATE UNIQUE INDEX uq_pesel ON users(pesel) WHERE pesel IS NOT NULL;

-- Tabela kandydatów
CREATE TABLE elections (
	election_id INTEGER DEFAULT NEXTVAL('elections_seq') PRIMARY KEY NOT NULL,
	name VARCHAR(64) NOT NULL,
	description TEXT NOT NULL,
	start_date timestamp NOT NULL,
	end_date timestamp NOT NULL,
	candidates_deadline timestamp NOT NULL,
	places_limit INT DEFAULT 1,
	votes_limit INT DEFAULT 1,
	is_published BOOLEAN DEFAULT FALSE,
	CHECK(is_published = FALSE OR end_date < NOW() ), 
	CHECK( -- poprawność dat
		start_date < end_date AND candidates_deadline < start_date
	),
	CHECK( votes_limit >= 1 ),
	CHECK( places_limit >= 1)
);

CREATE TABLE candidates (
	candidate_id INTEGER DEFAULT NEXTVAL('candidates_seq') PRIMARY KEY NOT NULL,
	election_id INT NOT NULL REFERENCES elections,
	user_id VARCHAR(32) NOT NULL REFERENCES users,
	is_accepted BOOLEAN DEFAULT false -- czy kandydat został zaakceptowany
);

-- w jednych wyborach dany student może wystartować tylko jako jeden kandydat.
CREATE UNIQUE INDEX el_us_uq ON candidates(election_id,user_id);

CREATE TABLE votes (
	vote_id INTEGER DEFAULT NEXTVAL('votes_seq') PRIMARY KEY NOT NULL,
	candidate_id INT NOT NULL REFERENCES candidates,
	user_id VARCHAR(32) NOT NULL REFERENCES users,
	timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- użytkownik może zagłosować raz na konkretnego kandydata.
CREATE UNIQUE INDEX cand_user_uq ON votes(candidate_id,user_id);

CREATE TABLE log (
	id SERIAL PRIMARY KEY NOT NULL,
	index_number VARCHAR(32) NOT NULL,
	login_time timestamp NOT NULL
);