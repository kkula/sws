<?php

$admin = $app['controllers_factory'];

$admin->before(function() use ($app) {
	$user = $app['session']->get('user');
	if(!$user || !$user['is_admin']) {
		$app['view.name'] = 'error/access-denied';
		return ' ';
	}
});

$admin->get('/',function() use($app) {
	$app['view.name'] = 'admin/index';
	$app['view.vars']->elections = $app['db.elections']->getAllElections();
});

$admin->get('/publish/{id}',function($id) use ($app) {
	$app['db.elections']->publishElection($id);
	return $app->redirect($app['vvars']['base_path'].'admin');
});

$admin->get('/unpublish/{id}',function($id) use ($app) {
	$app['db.elections']->unpublishElection($id);
	return $app->redirect($app['vvars']['base_path'].'admin');
});

$admin->get('/elections/{id}',function($id) use ($app) {
	$app['view.name'] = 'admin/elections';
	$app['view.vars']->candidates = $app['db.elections']->getCandidates($id);
});

$admin->get('/accept-candidate/{id}',function($id) use ($app) {
	return $app->redirect($app['vvars']['base_path'].'admin/elections/' . $app['db.elections']->acceptCandidate($id));
});

$admin->get('/new-elections',function() use($app) {
	$app['view.name'] = 'admin/new-election';
	$app['view.vars']->msg = $app['session']->get('msg');
	$form = $app['session']->get('form');
	if(!$form) {
		$form = array('name' => '','description' => '','start_date' => '','end_date' => '','places_limit' => '','votes_limit' => '', 'candidates_date' => '');
	}
	$app['view.vars']->form = $form;
	$app['session']->remove('msg');
	$app['session']->remove('form');
});

$admin->post('/new-elections',function() use($app) {
	if($app['db.elections']->addElections($_POST)) {
		return $app->redirect($app['vvars']['base_path'].'admin');
	} else {
		$app['session']->set('msg',array('error','Errors in form.'));
		$app['session']->set('form',$_POST);
		return $app->redirect($app['vvars']['base_path'].'admin/new-elections');
	}die;
});

$admin->get('/users',function() use ($app) {
	return $app->redirect($app['vvars']['base_path'].'admin/users/1');
});

$admin->get('/new-user',function() use ($app) {
	$app['view.name'] = 'admin/new-user';
	$app['view.vars']->msg = $app['session']->get('msg');
	$app['session']->remove('msg');
});

$admin->post('/new-user',function() use ($app) {
	if($app['db.users']->addUser($_POST)) {
		return $app->redirect($app['vvars']['base_path'].'admin/users');
	} else {
		$app['session']->set('msg',array('error','Errors in form.'));
		return $app->redirect($app['vvars']['base_path'].'admin/new-user');
	}
});

$admin->get('/users/{page}',function($page) use ($app) {
	if($page <= 0) {
		$app->redirect($app['vvars']['base_path'].'admin/users/1');
	}
	$app['view.name'] = 'admin/users';
	
	$app['view.vars']->users = $app['db.users']->getUsersPage($page);
	$app['view.vars']->page = $page;
	
});

$admin->get('/login-log',function() use ($app) {
	$app['view.name'] = 'admin/login-log';
	$app['view.vars']->logs = $app['db.users']->getLog();
});

return $admin;