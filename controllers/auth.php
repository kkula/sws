<?php

$auth = $app['controllers_factory'];

$auth->get('/',function() use($app) {
	$app['view.name'] = 'auth/index';
	if($app['session']->has('user'))
		return $app->redirect($app['vvars']['base_path']);
});

$auth->post('/',function(Symfony\Component\HttpFoundation\Request $req) use ($app) {
	$index = $req->get('index');
	$passwd = $req->get('passwd');
	if($app['db.users']->auth($index,$passwd)) {
		$app['session']->set('user',$app['db.users']->getUser($index));
		return $app->redirect($app['vvars']['base_path']);
	}
	$app['view.name'] = 'auth/index';
	$app['view.vars']->message = 'Zły numer indeksu lub hasło.';
});

$auth->get('/logout',function() use ($app) {
	$app['session']->remove('user');
	return $app->redirect($app['vvars']['base_path']);
});

return $auth;