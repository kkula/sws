<?php

$elections = $app['controllers_factory'];

$elections->before(function() use ($app) {
	$user = $app['session']->get('user');
	if(!$user) {
		$app['view.name'] = 'error/access-denied';
		return ' ';
	}
});

$elections->get('/result/{id}',function($id) use($app) {
	$app['view.name'] = 'elections/result';
	
	if(!$app['db.elections']->isPublished($id)) {
		die('Wyniki nie są jeszcze opublikowane!');
	}
	
	$c = $app['view.vars']->candidates = $app['db.elections']->getResults($id);
	
	$ch = array();
	foreach($c as $r) {
		$ch[] = array($r['name'] . ' ' . $r['surname'],$r['votes']);
	}
	
	$app['view.vars']->chart_json = json_encode($ch);
	
});

$elections->get('/',function() use ($app) {
	$user = $app['session']->get('user');
	$app['view.name'] = 'elections/list';
	$e = $app['db.elections']->getActiveElections($user['index_number']);
	$app['view.vars']->elections = array_chunk($e,3);
	$app['view.vars']->type = 'active';
	$app['view.vars']->msg = $app['session']->get('msg');
	$app['session']->remove('msg');
});

$elections->get('/future',function() use ($app) {
	$app['view.name'] = 'elections/list';
	$app['view.vars']->elections = array_chunk($app['db.elections']->getFutureElections(),3);
	$app['view.vars']->type = 'future';
	$app['view.vars']->now = time();
	$app['view.vars']->msg = $app['session']->get('msg');
	$app['session']->remove('msg');
});

$elections->get('/past',function() use ($app) {
	$app['view.name'] = 'elections/list';
	$app['view.vars']->elections = array_chunk($app['db.elections']->getPastElections(),3);
	$app['view.vars']->type = 'past';
});

$elections->get('/join/{elections}',function($elections) use ($app) {
	$user = $app['session']->get('user');
	if($app['db.elections']->addCandidate($elections,$user['index_number'])) {
		$app['session']->set('msg',array('success','Zgłoszono kandydaturę.'));
	} else {
		$app['session']->set('msg',array('error', 'Jesteś już kandydatem do tych wyborów.'));
	}
	
	return $app->redirect($app['vvars']['base_path'].'elections/future');
});

$elections->get('/vote/{id}',function($id) use ($app) {
	$user = $app['session']->get('user');
	if($app['db.elections']->hasVoted($user['index_number'],$id)) {
		$app['view.name'] = 'error/has-voted';
		return;
	}
	
	$app['view.name'] = 'elections/vote';
	$elections = $app['db.elections']->getElections($id);
	$app['view.vars']->election = $elections;
	$app['view.vars']->candidates = $app['db.elections']->getAcceptedCandidates($id);
});

$elections->post('/vote/{id}',function($id) use ($app) {
	$user = $app['session']->get('user');
	if($app['db.elections']->hasVoted($user['index_number'],$id)) {
		$app['view.name'] = 'error/has-voted';
		return;
	}
	
	$elections = $app['db.elections']->getElections($id);
	
	$votes = $_POST['vote'];
	
	if(count($votes) > $elections['votes_limit'])
		die('Too many votes');
	
	foreach($votes as $vote) {
		// check if vote belongs to election.
		if($app['db.elections']->getCandidatesElection($vote) != $id)
			die('Don\'t try to cheat.');
	}
	
	
	// everythings fine. Now we may register votes.
	
	foreach($votes as $vote) {
		$app['db.elections']->addVote($user['index_number'],$vote);
	}
	
	$app['session']->set('msg',array('success','Oddano głos na ' . count($votes) . ' kandydatów. Dziękujemy!'));
	
	return $app->redirect($app['vvars']['base_path'].'elections/');
	
});

return $elections;