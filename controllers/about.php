<?php

$about = $app['controllers_factory'];

$about->get('/',function() use ($app) {
	$app['view.name'] = 'about/main_page';
});

$about->get('/about/tech',function() use ($app) {
	$app['view.name'] = 'about/tech';
});

$about->get('/about/erd',function() use ($app) {
	$app['view.name'] = 'about/erd';
});

$about->get('/about/sql',function() use ($app) {
	$app['view.name'] = 'about/sql';
	$app['view.vars']->sql = file_get_contents('../db/1_structure.sql');
	$app['view.vars']->header = 'SQL: Struktura';
});

$about->get('/about/views',function() use ($app) {
	$app['view.name'] = 'about/sql';
	$app['view.vars']->sql = file_get_contents('../db/2_views.sql');
	$app['view.vars']->header = 'SQL: Widoki';
});

$about->get('/about/triggers',function() use ($app) {
	$app['view.name'] = 'about/sql';
	$app['view.vars']->sql = file_get_contents('../db/6_triggers.sql');
	$app['view.vars']->header = 'SQL: Wyzwalacze';
});

$about->get('/about/procedures',function() use ($app) {
	$app['view.name'] = 'about/sql';
	$app['view.vars']->sql = file_get_contents('../db/5_procedures.sql');
	$app['view.vars']->header = 'SQL: Procedury';
});

$about->get('/about/data',function() use ($app) {
	$app['view.name'] = 'about/data';
});

$about->get('/about/author',function() use ($app) {
	$app['view.name'] = 'about/author';
});

$about->get('/reset', function() use ($app) {
			$location = realpath(__DIR__ . '/../db/');
			foreach (glob($location . '/*.sql') as $file) {
				echo $file;
				$status = $app['db.connection']->exec(file_get_contents($file));
				if (!$status) {
					echo " [OK]<br/>";
				} else {
					echo " [ERR]<br/>";
				}
			}
		});

return $about;