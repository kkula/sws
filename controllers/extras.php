<?php

$cont = $app['controllers_factory'];

$cont->get('/generate',function() use ($app) {
	$data = explode(PHP_EOL,file_get_contents('../db/3_data_users.sql'));
	array_shift($data);
	foreach($data as &$row) {
		$row = trim($row,'(),');
		$row = (int)trim(array_shift(explode(',',$row)),"'");
	}
	
	$elections = array(1,2,3,4);
	
	
	function random_val($arr,$count) {
		$keys = array_rand($arr,$count);
		$r = array();
		
		if($count == 1)
			return $arr[$keys];
		
		foreach($keys as $key) {
			$r[] = $arr[$key];
		}
		return $r;
	};
	
	$candidate_id = 1;
	
	foreach($elections as $e_id) {
		
		$candidates_count = rand(10,40);
		$all_candidates = random_val($data, $candidates_count);
		
		
		$candidates_unaccepted = random_val($all_candidates,rand(3,10));
		$candidates = array_diff($all_candidates, $candidates_unaccepted);
		
		
		echo 'INSERT INTO candidates(candidate_id,election_id,user_id,is_accepted) VALUES' . "<br/>";
		$cids = array();
		foreach ($candidates as $candidate) {
			$cids[] = $candidate_id;
			echo '(' . $candidate_id++ . ',' .  $e_id . ',\'' . $candidate . '\',TRUE),<br/>';
		}
		
		foreach($candidates_unaccepted as $candidate) {
			echo '(' . $candidate_id++ . ',' . $e_id . ',\'' . $candidate . '\',FALSE),<br/>';
		}
		
		
		// generating voters
		
		$voters_count = rand(1000,10000);
		
		$voters = random_val($data,$voters_count);
		
		echo 'INSERT INTO votes(candidate_id,user_id) VALUES<br/>';
		foreach($voters as $v) {
			if($v == 0)
				continue;
			// generate for who he votes.
			$candidate = random_val($cids,1);
			
			echo '(' . $candidate . ',\'' . $v . '\'),<br/>';
			
		}
		echo "<br/><br/>";
		
		
		
	}
});
	
	$cont->get('/extra-votes',function() use ($app) {
		$data = explode(PHP_EOL,file_get_contents('../db/3_data_users.sql'));
	array_shift($data);
	foreach($data as &$row) {
		$row = trim($row,'(),');
		$row = (int)trim(array_shift(explode(',',$row)),"'");
	}
	
	
	function random_val($arr,$count) {
		$keys = array_rand($arr,$count);
		$r = array();
		
		if($count == 1)
			return $arr[$keys];
		
		foreach($keys as $key) {
			$r[] = $arr[$key];
		}
		return $r;
	};
	
	
	$voters = random_val($data,100);
	
	
		
		echo 'INSERT INTO votes(candidate_id,user_id) VALUES<br/>';
		foreach($voters as $v) {
			if($v == 0)
				continue;
			// generate for who he votes.
			$candidate = 27;
			
			echo '(' . $candidate . ',\'' . $v . '\'),<br/>';
			
		}
	});

return $cont;